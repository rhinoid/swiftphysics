//
//  Ball.swift
//  Physics
//
//  Created by Reinier van Vliet on 06/02/15.
//  Copyright (c) 2015 Reinier van Vliet. All rights reserved.
//

import Foundation
import UIKit

class Ball : UIImageView {

    var posX = 100.0
    var posY = 200.0
    var oldX = 100.0
    var oldY = 200.0
    static var accX = 0.0
    static var accY = 0.9
    var groundResistance = 0.9
    
    var playingFieldFrame = CGRectZero
    
    var ballHelperProto : BallHelper?

    func update(){
        center = CGPoint(x: posX, y: posY)
    }
    
    let timeStep = 1.0
    
    func integrate() {
        let nextOldX = posX
        let nextOldY = posY
        
        posX += posX - oldX + Ball.accX * timeStep * timeStep
        posY += posY - oldY + Ball.accY * timeStep * timeStep
        oldX = nextOldX
        oldY = nextOldY
    }

    func calcConstraints() {
        if posX > Double(playingFieldFrame.size.width) {
            posX = Double(playingFieldFrame.size.width)
        }
        if posX < 0.0 {
            posX = 0.0
        }
        if posY > Double(playingFieldFrame.size.height) {
            posY = Double(playingFieldFrame.size.height)
        }
        if posY < 0.0 {
            posY = 0.0
        }
        
        if(ballHelperProto != nil){
            ballHelperProto?.handleCollision(self)
        }
    
    }
}
