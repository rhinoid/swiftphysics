//
//  ViewController.swift
//  Physics
//
//  Created by Reinier van Vliet on 16/01/15.
//  Copyright (c) 2015 Reinier van Vliet. All rights reserved.
//

import UIKit
import QuartzCore;

protocol BallHelper {
    func handleCollision(ball : Ball);
}

class ViewController: UIViewController, BallHelper {

    var displayLink : CADisplayLink!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        gameInit();
        
        displayLink = CADisplayLink(target: self, selector: Selector("gameLoop"))
        displayLink.frameInterval = 1
        displayLink.addToRunLoop(NSRunLoop.currentRunLoop(), forMode: NSRunLoopCommonModes)
        
        //detect orientation change
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "rotated:", name: "UIDeviceOrientationDidChangeNotification", object: nil);
    }

    var lastTouches : Set<NSObject>?
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        lastTouches = touches
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        lastTouches = nil
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        lastTouches = touches
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func rotated(notification : NSNotification){
        let orientation = UIDevice.currentDevice().orientation
        
        switch orientation {
        case UIDeviceOrientation.Portrait:
            Ball.accX = 0.0
            Ball.accY = 0.9
        case UIDeviceOrientation.PortraitUpsideDown:
            Ball.accX = 0.0
            Ball.accY = -0.9
        case UIDeviceOrientation.LandscapeLeft:
            Ball.accX = -0.9
            Ball.accY = 0.0
        case UIDeviceOrientation.LandscapeRight:
            Ball.accX = 0.9
            Ball.accY = 0.0
        default:
            break
        }
    }

    var numberOfBalls = 26
    let ballRadius = 22.0
    var balls: [Ball!]!
    
    func gameInit(){
        balls = [Ball]()
        let ballImage : UIImage = UIImage(named: "ball")!
        for (var i=0; i<numberOfBalls; i++){
            let ball = Ball(image: ballImage)
            ball.posX = ball.posX + Double(i*80)
            ball.posY = ball.posY + Double(i*50)
            ball.oldX = ball.posX-8     //the 8 is to give it an initial speed horizontally (since we don't have spdX anymore)
            ball.oldY = ball.posY
            ball.playingFieldFrame = view.frame
            ball.frame = CGRect(x: 0, y: 0, width: ballImage.size.width, height: ballImage.size.height)
            ball.ballHelperProto = self
            view.addSubview(ball)
            balls.append(ball)
        }
    }
    
    func gameLoop(){
        checkTouches()
        
        for (var i=0; i<numberOfBalls; i++){
            balls[i].integrate();
        }
        for (var j=0; j<3; j++){
            for (var i=0; i<numberOfBalls; i++){
                balls[i].calcConstraints()
            }
//            calcChain();
            calcCube();
        }
        for (var i=0; i<numberOfBalls; i++){
            balls[i].update();
        }
        
    }
    
    func checkTouches(){
        if let touches = lastTouches {
            var index = 0
            for touchObj in touches {
                if let touch = touchObj as? UITouch{
                    let point = touch.locationInView(view)
                    balls[index].posX = Double(point.x)
                    balls[index].posY = Double(point.y)
                    index++
                }
            }
        }
    }
    
    func calcChain() {
        for (var i=0; i<numberOfBalls-1; i++){
            let dx = balls[i].posX - balls[i+1].posX
            let dy = balls[i].posY - balls[i+1].posY
            let dist = (dx*dx) + (dy*dy);
            if(dist>0){
                enforceLength(balls[i], ball2: balls[i + 1], length: ballRadius)
            }
        }
    }
    
    func calcCube() {
        enforceLength(balls[0], ball2: balls[1], length: ballRadius)
        enforceLength(balls[1], ball2: balls[2], length: ballRadius)
        enforceLength(balls[2], ball2: balls[3], length: ballRadius)
        enforceLength(balls[3], ball2: balls[0], length: ballRadius)
        
        enforceLength(balls[0], ball2: balls[2], length: ballRadius * sqrt(2.0))
        enforceLength(balls[1], ball2: balls[3], length: ballRadius * sqrt(2.0))
    }
    
    func enforceLength(ball1 : Ball, ball2 : Ball, length: Double){
        let dx = ball1.posX - ball2.posX
        let dy = ball1.posY - ball2.posY
        let dist = (dx*dx) + (dy*dy);
        if(dist>0){
            let actualDistance = sqrt(dist)
            let diameter = length*2
            let scaleFactor =  diameter / actualDistance
            let dxToMove = scaleFactor * dx
            let dyToMove = scaleFactor * dy
            ball1.posX -= dx/2
            ball1.posY -= dy/2
            ball2.posX += dx/2
            ball2.posY += dy/2
            ball1.posX += dxToMove / 2
            ball1.posY += dyToMove / 2
            ball2.posX -= dxToMove / 2
            ball2.posY -= dyToMove / 2
        }
    }
    
    func handleCollision(ball : Ball){
        for (var i=0; i<numberOfBalls; i++){
            if(!balls[i].isEqual(ball)){
                let dx = balls[i].posX - ball.posX;
                let dy = balls[i].posY - ball.posY;
                let dist = (dx*dx) + (dy*dy);
                if(dist < (ballRadius*2)*(ballRadius*2)){
                    let actualDistance = sqrt(dist)
                    let diameter = ballRadius*2
                    let scaleFactor =  (actualDistance-diameter) / diameter
                    let dxToMove = scaleFactor * dx
                    let dyToMove = scaleFactor * dy
                    ball.posX += dxToMove/2;
                    ball.posY += dyToMove/2;
                    balls[i].posX -= dxToMove/2;
                    balls[i].posY -= dyToMove/2;

//                    if(true){
//                        var v1x = ball.posX - ball.oldX;
//                        var v1y = ball.posY - ball.oldY;
//                        var v2x = balls[i].posX - balls[i].oldX;
//                        var v2y = balls[i].posY - balls[i].oldY;
//                        
//                        var f1 = (ball.groundResistance * (dx * v1x + dy * v1y)) / dist
//                        var f2 = (ball.groundResistance * (dx * v2x + dy * v2y)) / dist
//                        
//                        v1x += f2 * dx - f1 * dx
//                        v2x += f1 * dx - f2 * dx
//                        v1y += f2 * dy - f1 * dy
//                        v2y += f1 * dy - f2 * dy
//                        
//                        ball.oldX = ball.posX - v1x
//                        ball.oldY = ball.posY - v1y
//                        balls[i].oldX = balls[i].posX - v2x
//                        balls[i].oldY = balls[i].posY - v2y
//                    }
                }
            }
        }
    }

}

